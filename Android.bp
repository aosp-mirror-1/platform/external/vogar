// Copyright (C) 2014 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Build dependencies.
// ============================================================

package {
    default_applicable_licenses: ["external_vogar_license"],
}

// Added automatically by a large-scale-change that took the approach of
// 'apply every license found to every target'. While this makes sure we respect
// every license restriction, it may not be entirely correct.
//
// e.g. GPL in an MIT project might only apply to the contrib/ directory.
//
// Please consider splitting the single license below into multiple licenses,
// taking care not to lose any license_kind information, and overriding the
// default license using the 'licenses: [...]' property on targets as needed.
//
// For unused files, consider creating a 'fileGroup' with "//visibility:private"
// to attach the license to, and including a comment whether the files may be
// used in the current project.
// See: http://go/android-license-faq
license {
    name: "external_vogar_license",
    visibility: [":__subpackages__"],
    license_kinds: [
        "SPDX-license-identifier-Apache-2.0",
        "SPDX-license-identifier-BSD",
    ],
    license_text: [
        "LICENSE",
    ],
}

java_import_host {
    name: "vogar-jsr305",
    jars: ["lib/jsr305.jar"],
}

java_import_host {
    name: "vogar-kxml-libcore-20110123",
    jars: ["lib/kxml-libcore-20110123.jar"],
}

// build vogar jar
java_library_host {
    name: "vogar-host-jar",
    srcs: ["src/**/*.java"],
    // Make the host jar file name to be vogar.jar due to in vogar-script it
    // expect the jar to be vogar.jar.
    stem: "vogar",
    java_resource_dirs: ["resources"],
    static_libs: [
        "caliper",
        "gson",
        "guava",
        "junit",
        "testng",
        "vogar-jsr305",
        "vogar-kxml-libcore-20110123",
    ],
    // Vogar uses android.jar.
    libs: [
        "android_current",
    ],
}

// copy vogar script
sh_binary_host {
    // Set filename to the same name as the stem of vogar-host-jar to align with
    // original make behavior.
    name: "vogar-script",
    src: "bin/vogar-android",
    filename: "vogar",
}

// User can build both vogar-host-jar and vogar-script just use vogar as target
// name. This could be replace after soong has implemented the wrapper function
// for java_library_host.
phony_rule {
    name: "vogar",
    phony_deps: [
        "vogar-host-jar",
        "vogar-script",
    ],
}

// build vogar tests jar
java_library_host {
    name: "vogar-tests",
    srcs: ["test/**/*.java"],
    static_libs: [
        "junit",
        "mockito",
        "objenesis",
        "vogar-host-jar",
    ],
}

// Run the tests using using run-vogar-tests.
java_genrule_host {
    name: "vogar-tests-result",
    srcs: [
        ":vogar-tests",
    ],
    cmd: "ANDROID_BUILD_TOP=$$(pwd) java -cp $(in) org.junit.runner.JUnitCore vogar.AllTests 2>&1 | tee $(out)",
    out: ["vogar-tests-result.txt"],
}

phony_rule {
    name: "run-vogar-tests",
    phony_deps: [
        "vogar-tests-result",
    ],
}
